/**
 * Copyright 2020 German Tischler-Höhle
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#if ! defined(LIBSECRECY_GPGMEINPUTSTREAMWRAPPER_HPP)
#define LIBSECRECY_GPGMEINPUTSTREAMWRAPPER_HPP

#include <libsecrecy/GPGMEErrorBase.hpp>

namespace libsecrecy
{
	struct GPGMEInputStreamWrapper : public GPGMEErrorBase
	{
		std::istream & istr;
		gpgme_data_cbs cbs;
		gpgme_data_t data;

		static ssize_t gpgmeread(void * handle, void * buffer, ::std::size_t size)
		{
			GPGMEInputStreamWrapper * me = reinterpret_cast<GPGMEInputStreamWrapper *>(handle);
			std::istream & istr = me->istr;

			if ( ! size )
				return 0;

			if ( istr.eof() )
				return 0;

			istr.read(reinterpret_cast<char *>(buffer),size);

			ssize_t const r = istr.gcount();

			if ( istr.bad() )
			{
				errno = EIO;
				return -1;
			}
			else
			{
				return r;
			}
		}

		GPGMEInputStreamWrapper(
			std::istream & ristr
		) : istr(ristr)
		{
			cbs.read = gpgmeread;
			cbs.write = nullptr;
			cbs.seek = nullptr;
			cbs.release = nullptr;

			gpg_error_t const err = gpgme_data_new_from_cbs(&data,&cbs,this);

			if ( err )
			{
				throw std::runtime_error("libsecrecy::GPGMEInputStreamWrapper: gpgme_data_new_from_cbs failed "+errorToString(err));
			}
		}

		~GPGMEInputStreamWrapper()
		{
			gpgme_data_release(data);
		}
	};
}
#endif
