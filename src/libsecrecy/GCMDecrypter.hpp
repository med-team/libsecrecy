/**
 * Copyright 2020 German Tischler-Höhle
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#if ! defined(LIBSECRECY_GCMDECRYPTER_HPP)
#define LIBSECRECY_GCMDECRYPTER_HPP

#include <libsecrecy/GCMCryptBase.hpp>
#include <libsecrecy/GCMFactoryBase.hpp>
#include <libsecrecy/GPGMEContext.hpp>
#include <libsecrecy/AESEncryptKey.hpp>

namespace libsecrecy
{
	struct GCMDecrypterInterface
	{
		virtual ~GCMDecrypterInterface() {}

		virtual void read(char * const cdata, ::std::size_t const len) = 0;
		virtual void read(uint8_t * data, ::std::size_t len) = 0;
		virtual void reset(uint64_t const) = 0;
		virtual std::string getPrefix() const = 0;
		virtual void readDigest() = 0;
		virtual std::size_t getDigestSize() const = 0;
		virtual std::size_t getBlockSize() const = 0;
	};
}

namespace libsecrecy
{
	template<typename _encrypt_key_type>
	struct GCMDecrypter : public GCMCryptBase<_encrypt_key_type>, public GCMDecrypterInterface
	{
		typedef _encrypt_key_type encrypt_key_type;
		typedef GCMCryptBase<encrypt_key_type> base_type;

		static ::std::size_t const blocksize = encrypt_key_type::blocksize;
		static ::std::size_t const digestsize = GCM_DIGEST_SIZE;

		private:
		std::istream & in;
		::std::size_t const inblocksize;
		std::shared_ptr<uint8_t[]> B;

		public:
		GCMDecrypter(
			std::istream & rin,
			std::shared_ptr<encrypt_key_type> & rpkey,
			std::shared_ptr<GCMIV> IV,
			std::shared_ptr<uint8_t[]> sauthdata,
			::std::size_t const l_authdata,
			uint64_t const countershift = 0,
			::std::size_t const rinblocksize = 1024
		)
		:
		  GCMCryptBase<encrypt_key_type>(rpkey,IV,sauthdata,l_authdata,countershift),
		  in(rin),
		  inblocksize(rinblocksize),
		  B(new uint8_t[inblocksize])
		{
		}

		std::size_t getBlockSize() const
		{
			return blocksize;
		}

		void reset(uint64_t const countershift)
		{
			base_type::reset(countershift);
		}

		std::string getPrefix() const
		{
			return base_type::getPrefix();
		}

		std::size_t getDigestSize() const
		{
			return digestsize;
		}

		void read(char * const cdata, ::std::size_t const len)
		{
			static_assert (
				sizeof(char) == sizeof(uint8_t),
				"sizeof(char) != sizeof(uint8_t)"
			);
			read(
				reinterpret_cast<uint8_t *>(cdata),
				len
			);
		}

		void read(uint8_t * data, ::std::size_t len)
		{
			if ( len % blocksize != 0 )
			{
				std::ostringstream ostr;
				ostr << "libsecrecy::GCMDecrypter::read: input block of size " << len << " is not a multiple of blocksize " << blocksize;
				throw std::runtime_error(ostr.str());
			}

			while ( len )
			{
				std::size_t const use = std::min(len,inblocksize);

				static_assert (
					sizeof(char) == sizeof(uint8_t),
					"sizeof(char) != sizeof(uint8_t)"
				);
				in.read(reinterpret_cast<char *>(B.get()),use);

				if ( ! in || in.gcount() != static_cast<std::streamsize>(use) )
				{
					std::ostringstream ostr;
					ostr << "libsecrecy::GCMDecrypter::read: failed to read " << use << " bytes to input stream";
					throw std::runtime_error(ostr.str());
				}

				gcm_decrypt(base_type::gcmctx,base_type::gcmkey,base_type::key.getContext(),encrypt_key_type::getCipherFunction(),use,data,B.get());

				data += use;
				len -= use;
			}
		}

		void readDigest()
		{
			uint8_t  digest[GCM_DIGEST_SIZE];
			uint8_t rdigest[GCM_DIGEST_SIZE];

			in.read(reinterpret_cast<char *>(&rdigest[0]),GCM_DIGEST_SIZE);

			if ( ! in || in.gcount() != GCM_DIGEST_SIZE )
			{
				std::ostringstream ostr;
				ostr << "libsecrecy::GCMDecrypter::read: failed to read " << GCM_DIGEST_SIZE << " bytes to input stream";
				throw std::runtime_error(ostr.str());
			}

			gcm_digest(base_type::gcmctx,base_type::gcmkey,base_type::key.getContext(),encrypt_key_type::getCipherFunction(), GCM_DIGEST_SIZE, &digest[0]);

			if ( ! std::equal(&digest[0],&digest[GCM_DIGEST_SIZE],&rdigest[0]) )
			{
				std::ostringstream ostr;
				ostr << "libsecrecy::GCMDecrypter::readDigest: digest mismatch";
				throw std::runtime_error(ostr.str());
			}
		}
	};
}

namespace libsecrecy
{
	struct GCMDecrypterFactory : public GCMFactoryBase
	{
		template<typename key_type>
		static std::shared_ptr<GCMDecrypterInterface> construct(
			std::istream & rout,
			RawKeyObject const & RK,
			std::shared_ptr<GCMIV> IV,
			std::shared_ptr<uint8_t[]> sauthdata,
			::std::size_t const l_authdata,
			uint64_t const countershift = 0,
			::std::size_t const routblocksize = 1024
		)
		{
			typename key_type::raw_key_type const & rawkey = dynamic_cast<typename key_type::raw_key_type const &>(*(RK.ptr));
			std::shared_ptr<key_type> K(new key_type(rawkey));

			std::shared_ptr<GCMDecrypterInterface> tptr(
				new GCMDecrypter(rout,K,IV,sauthdata,l_authdata,countershift,routblocksize)
			);
			return tptr;
		}

		static std::shared_ptr<GCMDecrypterInterface> construct(
			std::istream & rout,
			libsecrecy::GPGMEContext & context,
			std::string const & hexhash,
			std::shared_ptr<GCMIV> IV,
			std::shared_ptr<uint8_t[]> sauthdata,
			::std::size_t const l_authdata,
			uint64_t const countershift = 0,
			::std::size_t const routblocksize = 1024,
			RawKeyBaseCacheInterface * cache = nullptr
		)
		{
			RawKeyObject RK = obtainKey(hexhash,false /* do not resolve name */,context,cache);
			std::string const scipher = RK.cipher;
			std::string const keyname = RK.name;

			if ( scipher == "AES128" )
			{
				return construct<AES128EncryptKey>(rout,RK,IV,sauthdata,l_authdata,countershift,routblocksize);
			}
			else if ( scipher == "AES192" )
			{
				return construct<AES192EncryptKey>(rout,RK,IV,sauthdata,l_authdata,countershift,routblocksize);
			}
			else if ( scipher == "AES256" )
			{
				return construct<AES256EncryptKey>(rout,RK,IV,sauthdata,l_authdata,countershift,routblocksize);
			}
			else
			{
				std::ostringstream ostr;
				ostr << "libsecrecy::GCMDecrypterFactory:: unsupported cipher " << scipher;
				throw std::runtime_error(ostr.str());
			}
		}

		static std::shared_ptr<GCMDecrypterInterface> construct(
			std::istream & rout,
			std::string const & hexhash,
			std::shared_ptr<GCMIV> IV,
			std::shared_ptr<uint8_t[]> sauthdata,
			::std::size_t const l_authdata,
			uint64_t const countershift = 0,
			::std::size_t const routblocksize = 1024,
			RawKeyBaseCacheInterface * cache = nullptr
		)
		{
			libsecrecy::GPGMEContext context;
			return construct(rout,context,hexhash,IV,sauthdata,l_authdata,countershift,routblocksize,cache);
		}
	};
}

#endif
