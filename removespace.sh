#! /bin/bash
for i in `find src -regex .*\\\.[ch]pp` `find src -regex .*\\\.[ch]` ; do
	ORIG=`cat $i`
	PATCHED=`perl -p -e "s/(\s*)($)/\n/" < ${i}`
	
	if [ "$ORIG" != "$PATCHED" ] ; then
		echo "${PATCHED}" > ${i}
		# git add ${i}
		echo ${i}
	fi
done
